class CreateApplications < ActiveRecord::Migration[7.0]
  def change
    create_table :applications do |t|
      t.column :application_details, :json, using: 'application_details::JSON'
      t.string :seeker_id 
      t.string :job_id
      
      t.timestamps
    end
  end
end
