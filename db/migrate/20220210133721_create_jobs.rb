class CreateJobs < ActiveRecord::Migration[7.0]
  def change
    create_table :jobs do |t|
      t.column :job_details, :json, using: 'job_details::JSON'
      t.string :created_by
      t.datetime :expiration_date

      t.timestamps
    end
  end
end
